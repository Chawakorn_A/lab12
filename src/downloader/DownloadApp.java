package downloader;

import javax.swing.SwingUtilities;

/**
 * Launch the URL downloader application.
 * @author Chawakorn Aougsuk
 *
 */
public class DownloadApp {

	/**
	 * Main method to start the user interface.
	 */
	public static void main(String[] args) {
		

		final DownloaderUI ui = new DownloaderUI();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ui.run();
			}
		} );
	}
}
